.. image:: https://pyrocko.org/docs/current/_images/pyrocko_shadow.png
    :align: left

***********
pyrocko.org
***********

**Software for Seismology**

.. raw:: html

    <div style="clear:both;"></div>

Pyrocko is an open source seismology toolbox and library, written in the Python
programming language.  It can be utilized flexibly for a variety of geophysical
tasks, like seismological data processing and analysis, modelling of InSAR, GPS
data and dynamic waveforms, or for seismic source characterization.


.. raw:: html

   <div style="clear:both; width:100%; text-align:center; margin-top:3em; margin-bottom:2em;">
       <a href="https://www.uni-potsdam.de/en/geo"><img style="margin-left:1em; height:2em; display:inline-block;" src="_static/logos/up_int.png" /></a>
       <a href="https://www.dfg.de/"><img style="margin-left:1em; height:2em; display:inline-block;" src="_static/logos/dfg.svg" /></a>
       <a href="https://www.bridges.uni-kiel.de/"><img style="margin-left:1em; height:2em; display:inline-block;" src="_static/logos/bridges.svg" /></a>
       <a href="https://www.uni-kiel.de/"><img style="margin-left:1em; height:2em; display:inline-block;" src="_static/logos/cau.svg" /></a>
       <a href="https://www.gfz-potsdam.de/">
            <img style="margin-left:1em; height:2em; display:inline-block;" src="_static/logos/gfz.svg" />
       </a>
       <a href="https://www.uni-hamburg.de/"><img style="margin-left:1em; height:2em; display:inline-block;" src="_static/logos/uhh.svg" /></a>
       <a href="https://www.kaust.edu.sa/"><img style="margin-left:1em; height:2em; display:inline-block;" src="_static/logos/kaust.svg" /></a>
   </div>

The home base of Pyrocko is `UPCODES - University of Potsdam Code Development for Seismology <https://www.uni-potsdam.de/en/geo/research/general-geophysics/facilities/upcodes-lab>`_.

Pyrocko framework
=================

At its core, Pyrocko is a library and framework providing building blocks for
researchers and students wishing to develop their own applications.

.. raw:: html

    <div class="icon-button-group" style="margin-bottom:2em">

        <a href="docs/current/" class="icon-button">
            <i class="fa fa-book" aria-hidden="true"></i><br />
            Pyrocko manual
        </a>

        <a href="docs/current/install/" class="icon-button">
            <i class="fa fa-download" aria-hidden="true"></i><br />
            Download and installation
        </a>

        <a href="https://git.pyrocko.org/pyrocko" class="icon-button">
            <i class="fa fa-code" aria-hidden="true"></i><br />
            Git repository<sup>1</sup>
        </a>

        <a href="https://git.pyrocko.org/pyrocko/pyrocko/issues" class="icon-button">
            <i class="fa fa-question" aria-hidden="true"></i><br />
            Support<sup>1</sup>
        </a>

        <a href="docs/current/library/examples/" class="icon-button">
            <i class="fa fa-graduation-cap" aria-hidden="true"></i><br />
            Examples / Notebooks
        </a>

        <a href="https://hive.pyrocko.org/" target="_blank" class="icon-button">
            <i class="fa fa-comments" aria-hidden="true"></i><br />
            Community chat<sup>2</sup>
        </a>

    </div>

| :sup:`1` To get an account for **git repository** and **support**, please send an email to `info@pyrocko.org <mailto:info@pyrocko.org>`_.
| :sup:`2` For the **community chat** you can self-register.

News
====

.. toctree::
    :maxdepth: 1

    2021-10-04: Pyrocko has a new home base: UPCODES at the University of Potsdam <news/2021-10-04>
    2021-03-04: Pyrocko Workshop at DGG 2021 <news/2021-03-04>
    2021-02-25: Pyrocko-Box: a VM image with Pyrocko preinstalled <news/2021-02-25>
    2020-01-16: A new tool to quickly visualize geophysical datasets: the Sparrow <news/2020-01-16>
    2019-08-05: Pyrocko is leaving GitHub <news/2019-08-05>


Applications
============

This section lists some applications built on top of the Pyrocko library. Some
of these may be useful for everyday seismological practice and are `included in
the Pyrocko package <docs/current/apps/>`_. Some others are tightly integrated
with Pyrocko for specialized tasks and can be found in their own software
repositories.

Snuffler
--------
.. raw:: html

    <div class="application-information">
        <span class="tag">
            <i class="fa fa-tag" aria-hidden="true"></i>
            <a href="docs/current/apps/snuffler/">Part of Pyrocko</a>
        </span>
    </div>

*Seismogram browser and workbench*

.. image:: _static/snuffler_screenshot.png
    :align: left

The Snuffler is an interactive and extensible seismogram browser that is suited for small and very big datasets and archives. It features plugins (called `Snufflings
<docs/current/apps/snuffler/extensions.html>`_), which are helpful
for broad variety of seismological applications. Features include:

* Event and phase picking (manual & STA/LTA)
* Spectral- and FK-analysis
* Beamforming
* Cross-correlation of traces

.. raw:: html

    <div class="application-tags">
        <span class="tag">
            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
            <a href="docs/current/apps/snuffler/tutorial.html">Tutorial</a>
        </span>
        <span class="tag">
            <i class="fa fa-puzzle-piece" aria-hidden="true"></i>
            <a href="https://git.pyrocko.org/pyrocko/contrib-snufflings">User contributed Snufflings</a>
        </span>
        <span class="tag doi">
            <i class="fa fa-university" aria-hidden="true"></i>
            <a href="https://doi.org/10.5880/GFZ.2.1.2017.001" target="_blank">DOI: 10.5880/GFZ.2.1.2017.001</a>
        </span>
    </div>

Cake
--------
.. raw:: html

    <div class="application-information">
        <span class="tag">
            <i class="fa fa-tag" aria-hidden="true"></i>
            <a href="docs/current/apps/cake/">Part of Pyrocko</a>
        </span>
    </div>

*1D travel-time and ray-path computations*

.. image:: _static/cake_plot_example.png
    :align: left

Cake is a very tasty tool that can be used to solve classical seismic ray theory problems
for layered-earth models (layer cake models). For various seismic phases it can
calculate:

* Arrival times
* Ray paths
* Reflection and transmission coefficients
* Take-off and incidence angles

Computations are done for a spherical earth.

.. raw:: html

    <div class="application-tags">
        <span class="tag doi">
            <i class="fa fa-university" aria-hidden="true"></i>
            <a href="https://doi.org/10.5880/GFZ.2.1.2017.001" target="_blank">DOI: 10.5880/GFZ.2.1.2017.001</a>
        </span>
    </div>

Fomosto
--------
.. raw:: html

    <div class="application-information">
        <span class="tag">
            <i class="fa fa-tag" aria-hidden="true"></i>
            <a href="docs/current/apps/fomosto/">Part of Pyrocko</a>
        </span>
    </div>

*Calculate and manage Green's function databases*

.. image:: _static/fomosto2.png
    :align: left

Calculation of Green’s functions for synthetic seismograms is a computationally
expensive operation and it can be of advantage to calculate and store them in
advance. Now, for typical application scenarios, the Green’s function traces
can be reused as required. Fomosto offers building of flexible Green's function
databases that can be shared and passed to other researchers, allowing them to
focus on their own application rather then spending days of work to get their
Green’s function setup ready.

.. raw:: html

    <div class="application-tags">
        <span class="tag" style="font-weight: 500;">
            <i class="fa fa-database" aria-hidden="true"></i>
            <a href="https://greens-mill.pyrocko.org">Green's Mill - Online resource for pre-calculated Green's functions</a>
        </span>
        <span class="tag doi">
            <i class="fa fa-university" aria-hidden="true"></i>
            <a href="https://doi.org/10.5880/GFZ.2.1.2017.001" target="_blank">DOI: 10.5880/GFZ.2.1.2017.001</a>
        </span>
    </div>

Jackseis
--------
.. raw:: html

    <div class="application-information">
        <span class="tag">
            <i class="fa fa-tag" aria-hidden="true"></i>
            <a href="docs/current/apps/jackseis/">Part of Pyrocko</a>
        </span>
    </div>

*Waveform archive data manipulation*

.. image:: _static/jackseis_250x172.png
    :align: left

Jackseis is a command-line tool for common manipulations of archived waveform
datasets. Have it in your pocket to do:

* File format conversions
* Dataset conversions between day-files, hour-files, etc.
* Batch replacement of waveform meta-information
* Flexible filename and directory hierarchy manipulations

.. raw:: html

    <div class="application-tags">
        <span class="tag doi">
            <i class="fa fa-university" aria-hidden="true"></i>
            <a href="https://doi.org/10.5880/GFZ.2.1.2017.001" target="_blank">DOI: 10.5880/GFZ.2.1.2017.001</a>
        </span>
    </div>

Grond
-----
.. raw:: html

    <div class="application-information application-standalone">
        <span class="tag">
            <i class="fa fa-cube" aria-hidden="true"></i>
            <a href="https://git.pyrocko.org/pyrocko/grond">Download and Documentation</a>
        </span>
    </div>

*Probabilistic source optimization*

.. image:: _static/fomosto_synthetic.png
    :align: left

Grond is a bootstrap-based probabilistic battering ram to explore and
efficiently converge in solution spaces of earthquake source parameter
estimation problems. Grond supports joint inversion of seismic and geodetic data sets.

* Inversion of point sources and finite rupture models
* Parameter trade-off analysis
* Integrated robust waveform data preprocessing
* Highly flexible objective function design
* Extensive HTML optimization reports

.. raw:: html

    <div class="application-tags">
        <span class="tag">
            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
            <a href="/grond/docs/current/">Tutorials and Documentation</a>
        </span>
        <span class="tag doi">
            <i class="fa fa-university" aria-hidden="true"></i>
            <a href="https://doi.org/10.5880/GFZ.2.1.2018.003" target="_blank">DOI: 10.5880/GFZ.2.1.2018.003</a>
        </span>
    </div>


BEAT
----
.. raw:: html

    <div class="application-information application-standalone">
        <span class="tag">
            <i class="fa fa-github" aria-hidden="true"></i>
            <a href="https://github.com/hvasbath/beat">Download and Installation</a>
        </span>
    </div>

*Bayesian Earthquake Analysis Tool*

.. image:: _static/mt_correlation.png
    :align: left

Flexible, bayesian, multi-dataset deformation source optimization including model uncertainties.
Probabilistic source inversion for point sources, extended sources up to distributed slip.

* Convergence insurance through Sequential Monte Carlo algorithm
* Option to include model uncertainties (theory errors due to Earth structure)
* Parallel sampling on available CPUs
* Optional GPU sampling for linear problems

.. raw:: html

    <div class="application-tags">
        <span class="tag">
            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
            <a href="https://pyrocko.org/beat/">Tutorials and Documentation</a>
        </span>
        <span class="tag doi">
            <i class="fa fa-university" aria-hidden="true"></i>
            <a href="https://doi.org/10.5880/fidgeo.2019.024" target="_blank">DOI: 10.5880/fidgeo.2019.024</a>
        </span>
    </div>


Kite
----

.. raw:: html

    <div class="application-information application-standalone">
        <span class="tag">
            <i class="fa fa-github" aria-hidden="true"></i>
            <a href="https://github.com/pyrocko/kite">Download and Installation</a>
        </span>
    </div>

*InSAR displacement analysis and postprocessing*

.. image:: _static/spool_screenshot.png
    :align: left

Get your InSAR displacement maps handled the Pyrocko way (and prepared for the deformation source analysis in Pyrocko). Experience a highly interactive inspection of static displacement fields and data noise. Do easy quadtree data subsampling and data error variance-covariance estimation of InSAR data for proper data weighting in deformation source optimizations.

.. raw:: html

    <div class="application-tags">
        <span class="tag">
            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
            <a href="/kite/docs/current/">Tutorials and Documentation</a>
        </span>
        <span class="tag doi">
            <i class="fa fa-university" aria-hidden="true"></i>
            <a href="http://doi.org/10.5880/GFZ.2.1.2017.002">DOI: 10.5880/GFZ.2.1.2017.002</a>
        </span>
    </div>


Talpa
-----

.. raw:: html

    <div class="application-information">
        <span class="tag">
            <i class="fa fa-tag" aria-hidden="true"></i>
            <a href="https://github.com/pyrocko/kite">Part of Kite</a>
        </span>
    </div>

*Interactive static displacement modelling*

.. image:: _static/talpa_screenshot.png
    :align: left


Fault ruptures and volcanic plumbing systems are complex and highly interactive processes which take place in heterogeneous composition of the Earth’s crust. To intuitively study the complexities, we developed a graphical tool to interact and link observed surface displacements with deformation sources. This may guide as a first measure and constrain future finite numerical optimisation. Talpa, the mole, provides interfaces to different displacement codes and models, one being ``pyrocko.gf``.

.. raw:: html

    <div class="application-tags">
        <span class="tag">
            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
            <a href="/kite/docs/current/tools/talpa.html">Examples and Documentation</a>
        </span>
        <span class="tag doi">
            <i class="fa fa-university" aria-hidden="true"></i>
            <a href="http://doi.org/10.5880/GFZ.2.1.2017.002">DOI: 10.5880/GFZ.2.1.2017.002</a>
        </span>
    </div>


.. _publications:

Publications
============

    Heimann, Sebastian; Kriegerowski, Marius; Isken, Marius; Cesca, Simone; Daout, Simon; Grigoli, Francesco; Juretzek, Carina; Megies, Tobias; Nooshiri, Nima; Steinberg, Andreas; Sudhaus, Henriette; Vasyura-Bathke, Hannes; Willey, Timothy; Dahm, Torsten (2017): **Pyrocko - An open-source seismology toolbox and library**. V. 0.3. GFZ Data Services. https://doi.org/10.5880/GFZ.2.1.2017.001


    Heimann, Sebastian; Kriegerowski, Marius; Dahm, Torsten; Simone, Cesca; Wang, Rongjiang: **A Green's function database platform for seismological research and education: applications and examples**. EGU General Assembly 2016, held 17-22 April, 2016 in Vienna Austria, p.15292


    Isken, Marius; Sudhaus, Henriette; Heimann, Sebastian; Steinberg, Andreas; Daout, Simon; Vasyura-Bathke, Hannes (2017): **Kite - Software for Rapid Earthquake Source Optimisation from InSAR Surface Displacement**. V. 0.1. GFZ Data Services. https://doi.org/10.5880/GFZ.2.1.2017.002

    Heimann, S., Vasyura-Bathke, H., Sudhaus, H., Isken, M.P., Kriegerowski, M., Steinberg, A., Dahm, T., 2019. **A Python framework for efficient use of pre-computed Green’s functions in seismological and other physical forward and inverse source problems**. Solid Earth, 2019, 10(6):1921–1935. https://doi.org/10.5194/se-10-1921-2019

    Vasyura-Bathke, Hannes; Dettmer, Jan; Steinberg, Andreas; Heimann, Sebastian; Isken, Marius; Zielke, Olaf; Mai, Paul Martin; Sudhaus, Henriette; Jónsson, Sigurjón: **The Bayesian Earthquake Analysis Tool**. Seismological Research Letters. https://doi.org/10.1785/0220190075

.. _material:

Material
========

* `Presentation from Pyrocko Workshop at DGG 2021 <https://data.pyrocko.org/presentations/pyrocko-workshop-dgg-2021/>`_
* `Pyrocko Overview Poster <https://data.pyrocko.org/material/pyrocko-poster-201709.pdf>`_
* `Pyrocko Green's Function Poster <https://data.pyrocko.org/material/pyrocko-gf-poster-2014.pdf>`_
* `Kite Overview and Earthquake Inversion Poster <https://data.pyrocko.org/material/kite-poster-201709.pdf>`_

.. meta::
    :description: Pyrocko is an open source seismology environment.
    :keywords: Seismology, Earthquake, Geodesy, Earth, Science, Software, Python, software development, open-source, modelling, waveforms, processing, insar, surface deformation
    :audience: scientists, students, researcher, software developer, universities, institutes
    :robots: index, follow
